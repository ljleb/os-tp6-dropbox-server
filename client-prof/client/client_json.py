#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys
import hashlib
import os
# import time
import binascii

import json


MAX_RECV = 1024 * 1024 *30
MAX_NOMBRE_RECV = 100

class Client:
    """
    Client
    """


    # Socket de connexion au serveur.
    connexion = None

    # --- CONSTRUCTEUR ---
    def __init__(self, host, port):
        "Connexion du client au serveur"

        self.connexion = socket.socket()
        # Se donner un objet de la classe socket.
        self.sock = socket.socket()

        # Fixé à non-blocking
        self.sock.settimeout(None)

        # Connexion au serveur
        self.sock.connect((host, port))

        # self.connexion.connect((host, port))

    def traitement_dossier(self, dossier):
        """Transformation d'un dossier en ajoutant '/' à la fin"""
        dossier_traite = dossier.replace("\\", '/')

        if dossier == '':
            dossier_traite = './'
        elif dossier_traite[-1] != '/':
            dossier_traite = dossier_traite + '/'

        return dossier_traite

    def verifie_syntaxe(self, msgApp):
        "Analyse la syntaxe du message du client"
        try:
            json.loads(msgApp)
            reponse = True
        except:
            reponse = False

        return reponse

    def recevoir_texte(self):
        "Attendre une réponse du serveur"

        compteur = 1
        texte_recu_total = ''

        while compteur < MAX_NOMBRE_RECV:
            texte_recu = self.sock.recv(MAX_RECV).decode(encoding='UTF-8')
            if compteur > 1:
                print('Attente du contenu... (' + str(compteur) + '/' + str(MAX_NOMBRE_RECV) + ')')
            compteur += 1
            texte_recu_total += texte_recu
            if self.verifie_syntaxe(texte_recu_total):
                break
            else:
                print("Attention, le texte reçu jusqu'à maintenant n'est pas syntaxiquement correct.")
                print("Attente de la suite...")
                print("(Faire CTRL-C s'il y a erreur)")
        if compteur >= MAX_NOMBRE_RECV:
            print("Réponse définitive: le texte du serveur reçu n'est pas syntaxiquement correct.")
            self.fermeture()
            sys.exit(1)

        return texte_recu_total

    def envoi_texte(self, msgClient):
        "Envoyer un texte au serveur"

        msgClient_rl = msgClient + "\r\n"
        self.sock.send(msgClient_rl.encode(encoding='UTF-8'))

    def envoyer_recevoir(self, requeteJSON):
        "Méthode pour envoyer et recevoir la réponse du serveur."

        self.envoi_texte(requeteJSON)

        reponseJSON = self.recevoir_texte()

        msgApp = reponseJSON
        # On coupe la longueur de la chaîne pour éviter l'affichage trop long.
        msgAppImp = (msgApp[:100] + '...') if len(msgApp) > 100 else msgApp
        print("Serveur: " + msgAppImp.strip())

        return reponseJSON

    def fermeture(self):
        "Fermeture de la connection avec le serveur"

        self.sock.close()

    def existeDossier(self, dossier):
        "Requête au serveur de l'existence d'un dossier"

        requeteJSON = json.dumps({ "questionListeDossiers": dossier })

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        reponseClient = False


        dom = json.loads(reponseJSON)

        # Traitement de listeDossiers
        if 'listeDossiers' in dom:
            reponseClient = True

        return reponseClient

    def listeDossier(self, dossier):
        "Requête au serveur pour obtenir la liste des sous-dossiers d'un dossier"

        # requeteJSON = json.dumps(json.loads({ "questionListeDossiers": dossier }))
        requeteJSON = json.dumps({ 'questionListeDossiers' : dossier })


        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        # reponseClient = []

        dom = json.loads(reponseJSON)

        # liste_dossiers = dom['listeDossiers']['dossier']
        liste_dossiers = dom['listeDossiers']['dossiers']

        return liste_dossiers

    def existeFichier(self, fichier, dossier):
        "Requête au serveur de l'existence d'un fichier"

        requeteJSON= json.dumps({ "questionListeFichiers": dossier })

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        reponseClient = False

        dom =  json.loads(reponseJSON)

        if 'listeFichiers' in dom:
            if 'fichier' in dom['listeFichiers']:
                reponseClient = True

        return reponseClient

    def listeFichiers(self, dossier):
        "Requête au serveur pour obtenir la liste des fichiers d'un dossier"

        requeteJSON= json.dumps({ "questionListeFichiers": dossier })


        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        dom = json.loads(reponseJSON)

        # liste_dossiers = dom['listeFichiers']['fichier']
        liste_dossiers = dom['listeFichiers']['fichiers']

        return liste_dossiers

    def creerDossier(self, dossier):
        "Requête de création d'un fichier sur le serveur"

        requeteJSON= json.dumps({ "creerDossier": dossier })

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        reponseClient = False

        dom = json.loads(reponseJSON)

        if 'reponse' in dom:
            if dom['reponse'] == 'ok':
                reponseClient = True

        return reponseClient

    def televerserFichier(self, nom, dossier):
        "Requête de téléversement d'un fichier."

        contenu = ""

        # Réponse par défaut.
        reponseClient = False

        fichier = dossier + nom
        contenu = open(fichier, 'rb').read()
        m = hashlib.sha256()
        # m = hashlib.md5()
        m.update(contenu)
        sign_serveur = m.hexdigest()

        # contenu_encode = base64.b32encode(contenu)
        contenu_encode = binascii.b2a_base64(contenu)

        contenu_ascii = ''
        if contenu_encode != '':
            contenu_ascii = contenu_encode.decode(encoding='ascii')

        fichier_stat = os.stat(fichier)
        date_modif = fichier_stat.st_mtime


        requeteJSON = json.dumps({ "televerserFichier": { "nom": nom,
                                                      "dossier": dossier,
                                                      "signature": sign_serveur,
                                                      "contenu": contenu_ascii,
                                                      "date": str(date_modif) }
                               })

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        reponseClient = False

        dom = json.loads(reponseJSON)

        if 'reponse' in dom:
            if dom['reponse'] == 'ok':
                reponseClient = True

        return reponseClient

    def telechargerFichier(self, nom, dossier):
        "Requête de téléchargement d'un fichier."

        requeteJSON = json.dumps({ "telechargerFichier": { "nom": nom,
                                                       "dossier": dossier,
                                                       } })
        # Réponse par défaut.
        reponseClient = False

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        dom = json.loads(reponseJSON)

        signature= dom['fichier']['signature']
        contenu = dom['fichier']['contenu']
        date_modif = dom['fichier']['date']


        # contenu_decode = base64.b32decode(contenu)
        try:
            contenu_decode = binascii.a2b_base64(contenu)
        except:
            contenu += '=' * (-len(contenu) % 4)
            contenu_decode = binascii.a2b_base64(contenu)

        fichier = dossier + nom

        if signature != "":
            # m = hashlib.md5()
            m = hashlib.sha256()
            m.update(contenu_decode)
            sign_serveur = m.hexdigest()

            if signature != sign_serveur:
                print("Erreur: Erreur de transfert du fichier " + fichier)

            obj_fichier = open(fichier, "wb")
            obj_fichier.write(contenu_decode)
            obj_fichier.close()

            fichier_stat = os.stat(fichier)
            date_acces = fichier_stat.st_atime
            # date_modif = fichier_stat.st_mtime

            os.utime(fichier,(date_acces,float(date_modif)))

        return reponseClient

    def supprimerFichier(self, nom, dossier):
        "Requête au serveur pour obtenir la liste des sous-dossiers d'un dossier"

        requeteJSON = json.dumps({ "supprimerFichier": { "nom": nom,
                                                     "dossier": dossier,
                                                     } })
        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        reponseClient = False

        dom = json.loads(reponseJSON)

        if 'reponse' in dom:
            if dom['reponse'] == 'ok':
                reponseClient = True

        return reponseClient

    def supprimerDossier(self, dossier):
        "Requête au serveur pour obtenir la liste des sous-dossiers d'un dossier"

        requeteJSON = json.dumps({ "supprimerDossier": dossier })

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        reponseClient = False

        dom = json.loads(reponseJSON)

        if 'reponse' in dom:
            if dom['reponse'] == 'ok':
                reponseClient = True

        return reponseClient

    def fichierRecent(self, nom, dossier):
        "Requête de comparaison des fichiers client et serveur."

        fichier = dossier + nom
        fichier_stat = os.stat(fichier)
        date_modif = fichier_stat.st_mtime

        requeteJSON = json.dumps({ "questionFichierRecent": { "nom": nom,
                                                          "dossier": dossier,
                                                          "date": str(date_modif) }
                               })

        # Réponse par défaut.
        reponseClient = False

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        dom = json.loads(reponseJSON)

        if 'reponse' in dom:
            if dom['reponse'] == 'oui':
                reponseClient = True

        return reponseClient

    def quitter(self):

        # requeteJSON = json.dumps({"action": "quitter"})
        requeteJSON = json.dumps({"quitter": ""})

        # Envoyer la requête et Attendre la réponse.
        reponseJSON = self.envoyer_recevoir(requeteJSON)

        # Réponse par défaut.
        reponseClient = False

        dom = json.loads(reponseJSON)

        # Traitement
        if 'reponse' in dom:
            if dom['reponse'] == 'bye':
                reponseClient = True

        return reponseClient

    def miseAjour(self, dossier):
        "Mise à jour de l'arborescence"

        # Parcours de l'arborescence sur le client...
        for racine, reps, noms in os.walk(dossier):

            # Corriger la syntaxe du dossier si nécessaire.
            racine_corrigee = self.traitement_dossier(racine)

            # Parcours des répertoires...
            for nom_rep in reps:

                # On crée le dossier si nécessaire.

                chemin_complet = racine_corrigee + nom_rep
                if not self.existeDossier(chemin_complet):
                    self.creerDossier(chemin_complet)

            # Parcours des répertoires...
            for nom_fichier in noms:

                # Si le fichier n'existe pas sur le serveur...
                if not self.existeFichier(nom_fichier, racine_corrigee):

                    self.televerserFichier(nom_fichier, racine_corrigee)

                else:

                    # Si le fichier existe sur le serveur...
                    if self.fichierRecent(nom_fichier, racine_corrigee):

                        os.remove(racine_corrigee + nom_fichier)
                        self.telechargerFichier(nom_fichier, racine_corrigee)

                    else:

                        self.supprimerFichier(nom_fichier, racine_corrigee)
                        self.televerserFichier(nom_fichier, racine_corrigee)

        # Liste des dossiers de départ
        liste_dossiers = [ dossier ]

        # Parcours de l'arborescence sur le serveur...
        while liste_dossiers:

            nom_dossier = liste_dossiers.pop()

            # Corriger la syntaxe du dossier si nécessaire.
            nom_dossier_corrigee = self.traitement_dossier(nom_dossier)

            nouveaux_dossiers = self.listeDossier(nom_dossier_corrigee)

            nouveaux_dossiers_racine = list()
            for dossier in nouveaux_dossiers:
                nouveaux_dossiers_racine.insert(0, nom_dossier_corrigee + dossier)

            liste_dossiers =  nouveaux_dossiers_racine + liste_dossiers

            # Si c'est un dossier...
            if not os.path.exists(nom_dossier_corrigee):
                # On crée le dossier localement.
                os.mkdir(nom_dossier_corrigee)

            for nom_fichier in self.listeFichiers(nom_dossier_corrigee):

                if not os.path.exists(nom_dossier_corrigee + nom_fichier):

                    self.telechargerFichier(nom_fichier, nom_dossier_corrigee)

        self.quitter()
