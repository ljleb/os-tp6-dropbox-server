from src.Server import Server
from src.CommandManager import CommandManager
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=int, help='port to open the server on')
    args = parser.parse_args()

    Server(('', int(args.port)), CommandManager()).start()