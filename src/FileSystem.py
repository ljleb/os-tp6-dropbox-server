#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import hashlib
import binascii
import sys
import zipfile
import math

class FileSystem:
    @staticmethod
    def read(path):
        with open(path, 'rb') as file:
            return binascii.b2a_base64(file.read()).decode('utf-8')

    @staticmethod
    def write(path, content):
        with open(path, 'wb') as file:
            file.write(binascii.a2b_base64(content.encode('utf-8')))

    @staticmethod
    def exists(path):
        return os.path.exists(path)

    @staticmethod
    def permits(path, mode=os.R_OK):
        return os.access(path, mode)

    @staticmethod
    def is_file(path):
        return os.path.isfile(path)

    @staticmethod
    def is_folder(path):
        return os.path.isdir(path)

    @staticmethod
    def folder_of(path):
        return os.path.dirname(path)

    @staticmethod
    def top_of(path):
        return os.path.basename(path)

    @staticmethod
    def mkdir(path):
        return os.mkdir(path)

    @staticmethod
    def rmdir(path):
        return os.rmdir(path)

    @staticmethod
    def remove_file(path):
        os.remove(path)
 
    @staticmethod
    def hash(path):
        return hashlib.sha256(FileSystem.read(path).encode('utf-8')).hexdigest()

    @staticmethod
    def date_of(path):
        return os.path.getmtime(path)

    @staticmethod
    def set_date_of(path, date):
        os.utime(path, (os.stat(path).st_atime, date))

    @staticmethod
    def is_updated(path, date):
        return int(float(date)) > int(FileSystem.date_of(path))

    @staticmethod
    def equals(path, hash, date):
        return int(float(date)) == int(FileSystem.date_of(path)) and FileSystem.hash(path) == hash

    @staticmethod
    def join(path, file_name):
        return os.path.join(path, file_name)

    @staticmethod
    def zip(path):
        if FileSystem.exists(path):
            FileSystem.remove_file(path)

        with zipfile.ZipFile(path, 'w') as zip_file:
            for folder, _, files in os.walk('.'):
                for file in files:
                    file = os.path.join(folder, file)
                    if file != path:
                        zip_file.write(folder, file)