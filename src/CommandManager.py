import os
from src.FileSystem import FileSystem
class CommandManager:

    def __init__(self):
        self.__commands = {
            'salutation': self.__greet_client,
            'questionNomServeur': self.__get_server_name,
            'questionListeDossiers': self.__get_list_folder,
            'creerDossier': self.__create_folder,
            'supprimerDossier': self.__delete_folder,
            'questionListeFichiers': self.__get_list_file,
            'questionFichierRecent': self.__file_is_more_recent,
            'questionFichierIdentique': self.__file_is_same,
            'televerserFichier': self.__download,
            'telechargerFichier': self.__upload,
            'supprimerFichier': self.__delete_file,
            'quitter': self.__quit,
            'archive': self.__archive
        }

    def __call__(self, key):
        return self.__commands[key]

    def __greet_client(self, body):
        return {'salutation': 'bonjourClient'}

    def __get_server_name(self, body):
        return {'nomServeur': os.uname().nodename}

    def __get_list_folder(self, body):
        if not FileSystem.exists(body):
            return {'reponse': 'erreurDossierInexistant'}

        elif not FileSystem.permits(body):
            return {'reponse': 'erreurDossierLecture'}

        return {
            'listeDossiers': {
                'dossiers': next(os.walk(body))[1]
            }
        }

    def __get_list_file(self, body):
        if not FileSystem.exists(body):
            return {'reponse': 'erreurDossierInexistant'}

        elif not FileSystem.permits(body):
            return {'reponse': 'erreurDossierLecture'}

        return {
            'listeFichiers': {
                'fichiers': next(os.walk(body))[2]
            }
        }

    def __create_folder(self, body):
        if not FileSystem.exists(FileSystem.folder_of(body)):
            return {'reponse': 'erreurDossierInexistant'}

        elif FileSystem.exists(body):
            return {'reponse': 'erreurDossierExistant'}

        elif not FileSystem.permits(FileSystem.folder_of(body)):
            return {'reponse': 'erreurDossierLecture'}

        FileSystem.mkdir(body)
        return {'reponse': 'ok'}

    def __delete_folder(self, body):
        if not FileSystem.exists(body):
            return {'reponse': 'erreurDossierInexistant'}

        elif not FileSystem.permits(body):
            return {'reponse': 'erreurDossierLecture'}

        FileSystem.rmdir(body)
        return {'reponse': 'ok'}

    def __file_is_more_recent(self, body):
        file = FileSystem.join(body['dossier'], body['nom'])
        if not FileSystem.exists(file):
            return {'reponse': 'erreurFichierInexistant'}

        elif not FileSystem.permits(file):
            return {'reponse': 'erreurFichierLecture'}

        return {'reponse': 'oui' if FileSystem.is_updated(file, body['date']) else 'non'}

    def __file_is_same(self, body):
        file = FileSystem.join(body['dossier'], body['nom'])
        if not FileSystem.exists(file):
            return {'reponse': 'erreurFichierInexistant'}

        elif not FileSystem.permits(file):
            return {'reponse': 'erreurFichierLecture'}

        return {'reponse': 'oui' if FileSystem.equals(file, body['signature'], body['date']) else 'non'}

    def __upload(self, body):
        file = FileSystem.join(body['dossier'], body['nom'])
        if not FileSystem.exists(file):
            return {'reponse': 'erreurFichierInexistant'}

        elif not FileSystem.permits(file):
            return {'reponse': 'erreurFichierLecture'}

        return {
            'fichier':
            {
                'signature': FileSystem.hash(file),
                'contenu': FileSystem.read(file),
                'date': FileSystem.date_of(file)
            }
        }

    def __download(self, body):
        file = FileSystem.join(body['dossier'], body['nom'])
        if FileSystem.exists(file):
            return {'reponse': 'erreurFichierExistant'}

        FileSystem.write(file, body['contenu'])
        return {'reponse': 'ok'}

    def __delete_file(self, body):
        file = FileSystem.join(body['dossier'], body['nom'])
        if not FileSystem.exists(file):
            return {'reponse': 'erreurFichierInexistant'}

        elif not FileSystem.permits(file):
            return {'reponse': 'erreurFichierLecture'}

        FileSystem.remove_file(file)
        return {'reponse': 'ok'}

    def __archive(self, body):
        archive_name = '__main__.zip'

        FileSystem.zip(archive_name)

        response = self.__upload({
            'nom': archive_name,
            'dossier': '.',
        })

        FileSystem.remove_file(archive_name)
        return response

    def __quit(self, body):
        return {'reponse': 'bye'}