import unittest
import os
import time
from src.CommandManager import CommandManager
from src.FileSystem import FileSystem

class CommandManagerTest(unittest.TestCase):

    COMMAND_MANAGER = CommandManager()
    TEST_PATH = FileSystem.join(FileSystem.join('.', 'src'), 'test')
    FOLDER = FileSystem.join(TEST_PATH, 'test_folder')
    NESTED_FOLDER = FileSystem.join(FOLDER, 'test_nested_folder')
    NESTED_NESTED_FOLDER = FileSystem.join(NESTED_FOLDER, 'test_nested_nested_folder')
    PROTECTED_FOLDER = FileSystem.join(FOLDER, 'test_protected_folder')
    INEXISTENT_FOLDER = FileSystem.join(TEST_PATH, 'test_inexistent_folder')
    FILE = FileSystem.join(FOLDER, 'test_file.txt')
    INEXISTENT_FILE = FileSystem.join(TEST_PATH, 'test_inexistent_file.txt')
    PROTECTED_FILE = FileSystem.join(PROTECTED_FOLDER, 'test_protected_file.txt')

    def test__greet__THEN__returnsGreetCommand(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('salutation')

        # Act
        response = command('bonjourServeur')

        # Assert
        ACTUAL_VALUE = response['salutation']
        EXPECTED_VALUE = 'bonjourClient'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__getServerName__THEN__returnsServerName(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionNomServeur')

        # Act
        response = command(str())

        # Assert
        ACTUAL_VALUE = response['nomServeur']
        EXPECTED_VALUE = os.uname().nodename
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__listFolders__IF__folderIsInCurrentFolder__THEN__folderIsInList(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionListeDossiers')

        # Act
        response = command(CommandManagerTest.FOLDER)

        # Assert
        ACTUAL_CONTAINER = response['listeDossiers']['dossiers']
        EXPECTED_MEMBER = FileSystem.top_of(CommandManagerTest.NESTED_FOLDER)
        self.assertIn(EXPECTED_MEMBER, ACTUAL_CONTAINER)

    def test__listFolders__IF__folderIsNotInCurrentFolder__THEN__folderIsNotInList(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionListeDossiers')

        # Act
        response = command(CommandManagerTest.FOLDER)

        # Assert
        ACTUAL_CONTAINER = response['listeDossiers']['dossiers']
        UNEXPECTED_MEMBER = CommandManagerTest.NESTED_NESTED_FOLDER
        self.assertNotIn(UNEXPECTED_MEMBER, ACTUAL_CONTAINER)

    def test__createFolder__IF__folderExists__THEN__returnsFailureStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('creerDossier')
        FileSystem.mkdir = lambda path: None

        # Act
        response = command(CommandManagerTest.FOLDER)

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'erreurDossierExistant'
        self.assertIn(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__createFolder__IF__folderDestinationDoesNotExist__THEN__returnsFailureStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('creerDossier')
        FileSystem.mkdir = lambda path: None

        # Act
        response = command(FileSystem.join(CommandManagerTest.INEXISTENT_FOLDER, CommandManagerTest.FOLDER))

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'erreurDossierInexistant'
        self.assertIn(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__createFolder__IF__folderDoesNotExist__THEN__returnsSuccessStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('creerDossier')
        FileSystem.mkdir = lambda path: None

        # Act
        response = command(CommandManagerTest.INEXISTENT_FOLDER)

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'ok'
        self.assertIn(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__deleteFolder__IF__folderDoesNotExist__THEN__returnsFailureStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('supprimerDossier')
        FileSystem.rmdir = lambda path: None

        # Act
        response = command(CommandManagerTest.INEXISTENT_FOLDER)

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'erreurDossierInexistant'
        self.assertIn(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__deleteFolder__IF__folderIsNotReadable__THEN__returnsFailureStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('supprimerDossier')
        FileSystem.rmdir = lambda path: None

        # Act
        response = command(CommandManagerTest.PROTECTED_FOLDER)

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'erreurDossierLecture'
        self.assertIn(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__deleteFolder__IF__folderExists__THEN__returnsSuccessStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('supprimerDossier')
        FileSystem.rmdir = lambda path: None

        # Act
        response = command(CommandManagerTest.FOLDER)

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'ok'
        self.assertIn(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsMoreRecent__IF__fileDoesNotExist__THEN__returnsFailureStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierRecent')

        # Act
        response = command({
            "nom": CommandManagerTest.INEXISTENT_FILE,
            "dossier": ".",
            "date": None,
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'erreurFichierInexistant'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsMoreRecent__IF__fileIsMoreRecent__THEN__returnsTrue(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierRecent')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": ".",
            "date": str(time.time()),
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'oui'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsMoreRecent__IF__fileIsLessRecent__THEN__returnsFalse(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierRecent')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": ".",
            "date": "0",
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'non'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsMoreRecent__IF__fileIsEquallyRecent__THEN__returnsFalse(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierRecent')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": ".",
            "date": FileSystem.date_of(CommandManagerTest.FILE),
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'non'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsSame__IF__fileDoesNotExist__THEN__returnsFailureStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierIdentique')

        # Act
        response = command({
            "nom": CommandManagerTest.INEXISTENT_FILE,
            "dossier": ".",
            "date": None,
            "signature": FileSystem.hash(CommandManagerTest.FILE),
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'erreurFichierInexistant'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsSame__IF__fileIsMoreRecent__THEN__returnsFalse(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierIdentique')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": ".",
            "date": str(time.time()),
            "signature": FileSystem.hash(CommandManagerTest.FILE),
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'non'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsSame__IF__fileIsLessRecent__THEN__returnsFalse(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierIdentique')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": ".",
            "date": "0",
            "signature": FileSystem.hash(CommandManagerTest.FILE),
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'non'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsSame__IF__hashIsDifferent__THEN__returnsFalse(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierIdentique')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": ".",
            "date": FileSystem.date_of(CommandManagerTest.FILE),
            "signature": "bad-signature",
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'non'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__fileIsSame__IF__hashIsDifferent__THEN__returnsTrue(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('questionFichierIdentique')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": ".",
            "date": FileSystem.date_of(CommandManagerTest.FILE),
            "signature": FileSystem.hash(CommandManagerTest.FILE),
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'oui'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__download__IF__fileDoesNotExist__THEN__returnsFailureStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('telechargerFichier')

        # Act
        response = command({
            "nom": CommandManagerTest.INEXISTENT_FILE,
            "dossier": "."
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'erreurFichierInexistant'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__download__IF__fileExists__THEN__returnsFile(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('telechargerFichier')

        # Act
        response = command({
            "nom": CommandManagerTest.FILE,
            "dossier": "."
        })

        # Assert
        ACTUAL_VALUE = response['fichier']
        EXPECTED_VALUE = {
            "signature": FileSystem.hash(CommandManagerTest.FILE),
            "contenu": FileSystem.read(CommandManagerTest.FILE),
            "date": FileSystem.date_of(CommandManagerTest.FILE),
        }
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)

    def test__upload__IF__fileDoesNotExist__THEN__returnsSuccessStatus(self):
        # Arrange
        command = CommandManagerTest.COMMAND_MANAGER('televerserFichier')
        FileSystem.write = lambda path, c: None

        # Act
        response = command({
            "nom": CommandManagerTest.INEXISTENT_FILE,
            "dossier": ".",
            "signature": FileSystem.hash(CommandManagerTest.FILE),
            "contenu": FileSystem.read(CommandManagerTest.FILE),
            "date": FileSystem.date_of(CommandManagerTest.FILE),
        })

        # Assert
        ACTUAL_VALUE = response['reponse']
        EXPECTED_VALUE = 'ok'
        self.assertEqual(EXPECTED_VALUE, ACTUAL_VALUE)