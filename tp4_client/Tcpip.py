#!/usr/bin/python3
# -*- coding: utf-8 -*-
import socket
import json
import sys


class Tcpip:

    def __init__(self, host, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(None)
        self.host = host
        self.port = port
        self.is_connected = False

    def connect(self):
        try:
            self.sock.connect((self.host, self.port))
            print('You are connected to host: ', self.host)
            self.is_connected = True
        except socket.error as e:
            print(str(e))

    def send(self, request):
        try:
            to_send = json.dumps(request) + "\r\n"
            self.sock.send(to_send.encode(encoding='UTF-8'))
        except socket.error as e:
            print('Message couldnt be sent')
            print(str(e))

    def receive(self):
        answer = ''
        try:
            answer = json.loads(self.sock.recv(4096).decode('utf-8'))
        except:
            print("Erreur de traitement du texte: " + answer)
            sys.exit(1)
        if answer == '':
            print("Le contenu JSON reçu du serveur est nonreconnu ou mal formaté.")
            sys.exit(1)
        return answer

    #Close connexion
    def fermeture(self):
        self.sock.close()